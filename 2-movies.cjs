const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/
//prob1

let keyArray = Object.keys(favouritesMovies);
let prob1Array = keyArray.map((element) => {
    let amount = +favouritesMovies[element].totalEarnings.substring(1, favouritesMovies[element].totalEarnings.length - 1);
    // console.log(amount);
    if (amount > 500) {
        // console.log(element);
        // console.log(favouritesMovies[element]);
        let tempOb = [];
        tempOb[element] = favouritesMovies[element];
        return tempOb; 
    }
});
console.log(prob1Array);

// prob2

    keyArray.map((ele) => {
    if(favouritesMovies[ele].oscarNominations > 3 && (+favouritesMovies[ele].totalEarnings.substring(1, favouritesMovies[ele].totalEarnings.length - 1)> 500))
    {
        // console.log(ele);
        // console.log(favouritesMovies[ele]);
    }
});

// prob3

    keyArray.map((ele) => {
        if(favouritesMovies[ele].actors.includes("Leonardo Dicaprio"))
        {
            // console.log(ele);
            // console.log(favouritesMovies[ele]);
        }
    })

// prob4

    sortedKeyArray = keyArray.sort((a,b) => {
        if(favouritesMovies[a].imdbRating > favouritesMovies[b].imdbRating)
        {
            return -1;
        }
        else if(favouritesMovies[a].imdbRating < favouritesMovies[b].imdbRating)
        {
            return 1;
        }
        else
        {
            if((+favouritesMovies[a].totalEarnings.substring(1, favouritesMovies[a].totalEarnings.length - 1)) > (+favouritesMovies[b].totalEarnings.substring(1, favouritesMovies[b].totalEarnings.length - 1)))
            {
                return -1;
            }
            else if((+favouritesMovies[a].totalEarnings.substring(1, favouritesMovies[a].totalEarnings.length - 1)) < (+favouritesMovies[b].totalEarnings.substring(1, favouritesMovies[b].totalEarnings.length - 1)))
            {
                return 1;
            }
            else{
                return 0;
            }
        }
    }).map((ele) => {
        // console.log(ele);
        // console.log(favouritesMovies[ele]);
    });

// prob5

keyArray.map((ele) => 
{


})